<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$pikachu = new Pokemon( "Pikachu", $electric, male, 30, 130, 100);
$mew = new Pokemon( "Mew", $psychic, null, 37, 105, 100);
$dragonite = new Pokemon( "Dragonite", $dragon, male, 270, 129, 100);
$sylveon = new Pokemon( "Sylveon", $fairy, female, 270, 129, 100);
$muk = new Pokemon("Muk", $poison, male, 250, 80, 100);
$darkrai = new Pokemon("Darkrai", $dark, male,220 , 250, 100);
$charizard = new Pokemon("Charizard", $fire, female, 210, 190, 100);
$blastoise = new Pokemon("Blastoise", $water, male, 240, 75, 100);
$venusaur = new Pokemon("Venusaur", $grass, male, 270, 50, 100);
$tyranitar = new Pokemon("Tyranitar", $dark, female, 205, 110, 100);
$aggron = new Pokemon("Aggron", $steel, female, 200, 45, 100);
$pidgeot = new Pokemon("Pidgeot", $flying, female, 190, 240, 100);
$lapras = new Pokemon("Lapras", $ice, female, 250, 150, 100);
$snorlax = new Pokemon("Snorlax", $normal, male, 280, 30, 100);
$lucario = new Pokemon("Lucario", $figting, male, 205, 210, 100);
$flygon = new Pokemon("Flygon", $ground, female, 210, 200, 100);
$bastiodon = new Pokemon("Bastiodon", $rock, male, 195, 50, 100);
$heracross = new Pokemon("Heracross", $bug, male, 170, 190, 100);
$gengar = new Pokemon("Gengar", $ghost, male, 260, 245, 100);
$sigilyph = new Pokemon("Sigilyph", $psychic, female, 210, 180, 100);
$greninja =  new Pokemon("Greninja", $water, male, 230, 255, 100);
$hawlucha =  new Pokemon("Hawlucha", $figting, male, 210, 215, 100);
$umbreon =  new Pokemon("Umbreon", $dark, female, 220, 200, 100);
$gyarados =  new Pokemon("Gyarados", $flying, male, 205, 197, 100);


$gengar->setAttack1($shadowBall);
$gengar->setAttack2($focusPunch4);
$gengar->setAttack3($stoneEdge);
$gengar->setAttack4($megaHorn);

$snorlax->setAttack1($bodySlam2);
$snorlax->setAttack2($focusPunch3);
$snorlax->setAttack3($earthquake3);
$snorlax->setAttack4($zenHeadbutt3);

$lapras->setAttack1($iceBeam3);
$lapras->setAttack2($blizzard);
$lapras->setAttack3($bodySlam);
$lapras->setAttack4($waterPulse);

$pidgeot->setAttack1($fly2);
$pidgeot->setAttack2($braveBird);
$pidgeot->setAttack3($extremeSpeed2);
$pidgeot->setAttack4($psyShock2);

$aggron->setAttack1($sunsteelStrike);
$aggron->setAttack2($zenHeadbutt2);
$aggron->setAttack3($darkPulse3);
$aggron->setAttack4($Outrage3);

$tyranitar->setAttack1($darkPulse2);
$tyranitar->setAttack2($earthquake2);
$tyranitar->setAttack3($rockSlide3);
$tyranitar->setAttack4($focusPunch2);

$venusaur->setAttack1($frenzyPlant);
$venusaur->setAttack2($earthquake);
$venusaur->setAttack3($rockSlide2);
$venusaur->setAttack4($hyperBeam2);

$blastoise->setAttack1($hydroPump);
$blastoise->setAttack2($iceBeam2);
$blastoise->setAttack3($dragonPulse2);
$blastoise->setAttack4($focusPunch);

$charizard->setAttack1($fireBlast2);
$charizard->setAttack2($dig);
$charizard->setAttack3($dragonPulse);
$charizard->setAttack4($fly);

$pikachu->setAttack1($thunderShock);
$pikachu->setAttack2($ironTail);
$pikachu->setAttack3($quickAttack);
$pikachu->setAttack4($electroBall);

$dragonite->setAttack1($extremeSpeed);
$dragonite->setAttack2($hyperBeam);
$dragonite->setAttack3($Outrage);
$dragonite->setAttack4($fireBlast);

$mew->setAttack1($megaPunch);
$mew->setAttack2($auraSphere);
$mew->setAttack3($gunkShot);
$mew->setAttack4($zenHeadbutt);

$sylveon->setAttack1($dazzlingGleam);
$sylveon->setAttack2($echoedVoice);
$sylveon->setAttack3($psyShock);
$sylveon->setAttack4($moonblast);

$muk->setAttack1($gunkShot2);
$muk->setAttack2($sludgeWave);
$muk->setAttack3($rockSlide);
$muk->setAttack4($knockOff);

$darkrai->setAttack1($dreamEater);
$darkrai->setAttack2($knockOff2);
$darkrai->setAttack3($darkPulse);
$darkrai->setAttack4($iceBeam);
