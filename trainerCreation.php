<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$pack1 = array($tyranitar, $darkrai, $gengar);
$pack2 = array($pidgeot, $aggron, $blastoise);
$pack3 =array($snorlax, $muk, $dragonite );
$pack4 =array($sylveon, $pikachu, $lapras );
$pack5 =array($venusaur, $charizard, $mew );
$davidKetchum = new Trainer("David_Ketchum",10, 23, "Pueblo_Paleta","Competitivo",$pack1);
$pabloKetchum = new Trainer("Pablo_Ketchum",12, 27, "Ciudad_Plateada","Competitivo",$pack2);
$luiggyKetchum = new Trainer("Luiggy_Ketchum",10, 21, "Ciudad_Carmin","Amistoso",$pack4);
$geraldineKetchum = new Trainer("Geraldine_Ketchum",10, 20, "Ciudad_Fucsia","Amistoso");
$dionisioKetchum = new Trainer("Dionisio_Ketchum",10, 22, "Isla_Canela","Competitivo");
$dianaKetchum = new Trainer("Diana_Ketchum",10, 20, "Ciudad_Azulona","Amistoso");
$zullyKetchum = new Trainer("Zully_Ketchum",11, 22, "Pueblo_Lavanda","Competitivo",$pack3);
$kevinKetchum = new Trainer("Kevin_Ketchum",10, 19, "Ciudad_Celeste","Aventajado",$pack5);
$davidKetcumMalvado = new Trainer("David_Ketchum_M",3, 23, "Pueblo_Paleta","Competitivo");
$pabloKetchumMalvado = new Trainer("Pablo_Ketchum_M",12, 27, "Ciudad_Plateada","Competitivo");
$luiggyKetchumMalvado = new Trainer("Luiggy_Ketchum_M",10, 21, "Ciudad_Carmin","Aventajado");
$geraldineKetchumMalvado = new Trainer("Geraldine_Ketchum_M",10, 20, "Ciudad_Fucsia","Competitivo");
$dionisioKetchumMalvado = new Trainer("Dionisio_Ketchum_M",3, 22, "Isla_Canela","Competitivo");
$dianaKetchumMalvado = new Trainer("Diana_Ketchum_M", 9, 20, "Ciudad_Azulona","Aventajado");
$zullyKetchumMalvado = new Trainer("Zully_Ketchum_M", 10, 22, "Pueblo_Lavanda","Competitivo");
$kevinKetchumMalvado = new Trainer("Kevin_Ketchum_M", 5, 19, "Ciudad_Celeste","Aventajado");