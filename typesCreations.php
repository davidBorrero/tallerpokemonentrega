<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$normalS=array("");
$normalW=array("fighting");
$normalI=array("gost");
//---------------------------------------------------------//
$fightingS=array("normal");
$fightingW=array("flying", "fairy", "psychic");
$fightingI=array("");
//---------------------------------------------------------//
$flyingS=array("bug","grass","fighting");
$flyingW=array("electric", "ice", "rock");
$flyingI=array("ground");
//---------------------------------------------------------//
$poisonS=array("grass", "fairy");
$poisonW=array("ground", "psychic");
$poisonI=array("");
//---------------------------------------------------------//
$groundS=array("fire", "electric", "poison", "rock", "steel");
$groundW=array("water", "grass", "ice");
$groundI=array("electric");
//---------------------------------------------------------//
$rockS=array("fire", "ice", "flying", "bug");
$rockW=array("water", "grass", "fighting", "ground", "steel");
$rockI=array("");
//---------------------------------------------------------//
$bugS=array("grass", "psychic", "dark");
$bugW=array("fire", "flying", "rock");
$bugI=array("");
//---------------------------------------------------------//
$ghostS=array("ghost", "psychic");
$ghostW=array("ghost", "dark");
$ghostI=array("normal", "fighting");
//---------------------------------------------------------//
$steelS=array("rock", "fairy");
$steelW=array("ground", "fighting", "fire");
$steelI=array("poison");
//---------------------------------------------------------//
$fireS=array("steel", "bug", "ice", "grass");
$fireW=array("rock", "water", "ground");
$fireI=array("");
//---------------------------------------------------------//
$waterS=array("fire", "ground", "rock");
$waterW=array("electric", "grass");
$waterI=array("");
//---------------------------------------------------------//
$grassS=array("water", "ground", "rock");
$grassW=array("fire", "ice", "poison", "flying", "bug");
$grassI=array("");
//---------------------------------------------------------//
$electricS=array("water", "flying");
$electricW=array("ground");
$electricI=array("");
//---------------------------------------------------------//
$psychicS=array("fighting", "poison");
$psychicW=array("bug", "ghost", "dark");
$psychicI=array("");
//---------------------------------------------------------//
$iceS=array("grass", "ground", "flying", "dragon");
$iceW=array("fire", "fighting", "rock", "steel");
$iceI=array("");
//---------------------------------------------------------//
$dragonS=array("dragon");
$dragonW=array("dragon", "ice", "fairy");
$dragonI=array("");
//---------------------------------------------------------//
$darkS=array("psychic", "ghost");
$darkW=array("fighting", "bug", "fairy");
$darkI=array("psychic");
//---------------------------------------------------------//
$fairyS=array("fighting", "dragon", "dark");
$fairyW=array("poison", "steel");
$fairyI=array("dragon");
//---------------------------------------------------------//


$normal = new TypePokemon(normal, $normalS, $normalW, $normalI);

$fighting = new TypePokemon(fighting, $fightingS, $fightingW, $fightingI);

$flying = new TypePokemon(flying, $flyingS, $flyingW, $flyingI);

$poison = new TypePokemon(poison, $poisonS, $poisonW, $poisonI);

$ground = new TypePokemon(ground, $groundS, $groundW, $groundI);

$rock = new TypePokemon(rock, $rockS, $rockW, $rockI);

$bug = new TypePokemon(bug, $bugS, $bugW, $bugI);

$ghost = new TypePokemon(ghost, $ghostS, $ghostW, $ghostI);

$steel = new TypePokemon(steel, $steelS, $steelW, $steelI);

$fire = new TypePokemon(fire, $fireS, $fireW, $fireI);

$water = new TypePokemon(water, $waterS, $waterW, $waterI);

$grass = new TypePokemon(grass, $grassS, $grassW, $grassI);

$electric = new TypePokemon(electric, $electricS, $electricW, $electricI);

$psychic = new TypePokemon(psychic, $psychicS, $psychicW, $psychicI);

$ice = new TypePokemon(ice, $iceS, $iceW, $iceI);

$dragon = new TypePokemon(dragon, $dragonS, $dragonW, $dragonI);

$dark = new TypePokemon(dark, $darkS, $darkW, $darkI);

$fairy = new TypePokemon(fairy, $fairyS, $fairyW, $fairyI);


