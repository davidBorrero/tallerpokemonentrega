<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Type
 *
 * @author user
 */
class TypePokemon {
    //put your code here
    static $name, $strengths, $weaknesses, $immunities;
    public function __construct($name, $strengths, $weaknesses, $immunities){
        $this->name = $name;
        $this->strengths = $strengths;
        $this->weaknesses = $weaknesses;
        $this->immunities = $immunities;
        
    }
    function getName(){
        return $this->name;
    }
    function getS(){
        return $this->strengths;
    }
    function getW(){
        return $this->weaknesses;
    }
    function getI(){
        return $this->immunities;
    }
    function pointOrComma($ar){
        for($i=0;$i<count($ar);$i++){
            echo $ar[$i];
            if(count($ar)>1){
                echo ", ";
            }else if($i===(count($ar)-1)){
                echo ". ";
            }
        }
    }
    function typeData(){
        
        $a_r1=$this->getS();
        $a_r2=$this->getW();
        $a_r3=$this->getI();
        echo $this->getName().":</br>";
        echo "Strong against-> ";
        $this->pointOrComma($a_r1);
        echo "</br>Weak against-> ";
        $this->pointOrComma($a_r2);
        echo "</br>Immune against-> ";
        $this->pointOrComma($a_r3);
        
    }
    
    
}


