<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pokemon
 *
 * @author pabhoz
 */
class Pokemon{
    //put your code here
    static $name, $type, $gender=null, $hp, $speed,  $level, $attacks;
    function __construct($name, $type, $gender, $hp, $speed, $level, $attacks="null"){
        if($attacks=="null" || $attacks==""){
            $attacks = array("null","null","null","null");
        }else{
            $this->attacks = $attacks;
        }
        $this->name = $name;
        $this->type = $type;
        $this->gender = $gender;
        $this->hp = $hp;
        $this->speed = $speed;
        $this->level = $level;
    }
    
    function getName(){
        return  $this->name;
    }
    
    function getType(){
        return $this->type;
    }
    
    function getGender(){
        return  $this->gender;
    }
    
    function getHp(){
        return  $this->hp;
    }
    
    function getSpeed(){
        return  $this->speed;
    }
    
    function getLevel(){
        return  $this->level;
    }
    
    function getAttack1(){
        return  $this->attacks[0];
    }
    
    function getAttack2(){
        return  $this->attacks[1];
    }
    function getAttack3(){
        return  $this->attacks[2];
    }
    function getAttack4(){
        return  $this->attacks[3];
    }
    
    function setAttack1($att){
        $this->attacks[0] = $att;
    }
    
    function setAttack2($att){
        $this->attacks[1] = $att;
    }
    
    function setAttack3($att){
        $this->attacks[2] = $att;
    }
    
    function setAttack4($att){
        $this->attacks[3] = $att;
    }
    
    function getAttacks(){
        return $this->attacks;
        echo "hola";
    }
    
    function printAttacks(){
        echo $this->getName()." tiene los siguientes ataques: </br>";
        echo "1->".$this->getAttack1()->name."</br>";
        echo "2->".$this->getAttack2()->name."</br>";
        echo "3->".$this->getAttack3()->name."</br>";
        echo "4->".$this->getAttack4()->name."</br></br>";
    }
}
