<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Battle
 *
 * @author user
 */
class Battle {
    //put your code here
    function __construct($trainerInput){
        $this->trainerInput = $trainerInput; 
    }
    
    
    
    function welcome(){
        echo "Bienvenidos al torneo Pokemon, donde solo los más fuertes podrán ganar.</br>";
        echo "Tengan listas sus pokeball, que la lucha dará inicio</br>";
        echo "Damos la bienvenida a los aspirantes al título, un fuerte aplauso para:</br> </br>";
        foreach ($this->trainerInput as $val){
            echo $val->name." </br>";
        }
        echo "</br>";
        echo "Formense para revisar que sean dignos de esta gran batalla, que inicien los TESTS!!! </br></br>";
    }
    function randomIze($values){
        $long=count($values);
        echo $long;
        $numUsed=array("null","null",);
        $vs;
        while (count($numUsed)<$long){
            $nr=rand(1, $long);
            for($i=0;$i<count(numUsed);$i++){
                if(numUsed[$i]==$nr){
                   echo "numero usado";
                }else{
                    echo "numero no usado";
                    array_push($numUsed, $nr);
                }
            }
        }
        
        
    }
    function combats(){
        echo "</br>";
        echo "<------------------------------------------------------------------------------------>";
        echo "</br>";
        echo "</br>";
        echo "</br>";
        echo "Que inicien los combates!!!!";
        echo "</br>";
        $nCombats = 0;
        $vv=0;
        $nameTrainer;
        foreach( $this->trainerInput as $value){
            if($value->able=="TRUE"){
                $vv++;
                $nameTrainer[$vv]=$value->name;
                echo "</br>";
                echo $nameTrainer[$vv];
                echo "</br>";
            }
            
        }
        echo "</br>";
        echo "Tenemos ".count($nameTrainer)." Participantes, listos para darlo todo y ganar!!.";
        echo "</br>";
        echo "Randomizando!!!!";
        echo "</br>";
        $this->randomIze($nameTrainer);
    }
    
    function test(){
        $this->welcome();
        foreach ($this->trainerInput as $val){
            if($val->medals >= 10){
                echo "<div style='color:green;'>".$val->name." tiene las medallas necesarias para participar"."</div>";
                
                echo "</br>";
                
            }else{
                echo "<div style='color:red;'>".$val->name." no tiene las medallas necesarias para participar, descalificado, lo sentimos"."</div>";
                $val->able =FALSE ;
                
                echo "</br>";
            }
        }
        echo "</br>";
        echo "Muy bien, terminamos la primera ronda de TESTS!!, continuemos";
        echo "</br>";
        echo "</br>";
        foreach ($this->trainerInput as $val){
            if(count($val->pokemon) >= 3){
                echo "<div style='color:green;'>".$val->name." tiene los pokemon necesarios para participar"."</div>";
                
                echo "</br>";
                
            }else{
                echo "<div style='color:red;'>".$val->name." no tiene los pokemon necesarios para participar, descalificado, lo sentimos"."</div>";
                $val->able =FALSE ;
                
                echo "</br>";
            }
        }
        echo "</br>";
        echo "Excelente, segunda ronda de TESTS!! terminada, continuemos";
        echo "</br>";
        echo "</br>";
        foreach ($this->trainerInput as $val){
            foreach ($val->pokemon as $vaal){
                if ($vaal->level == 100){
                    echo "<div style='color:green;'>".$val->name." tu pokemon ".$vaal->name." está al nivel adcuado: ".$vaal->level." </br>"."</div>";
                }else{
                    echo "<div style='color:red;'>".$val->name." tu pokemon ".$vaal->name." NO está al nivel adcuado: ".$vaal->level." </br>"."</div>";
                    echo "<div style='color:red;'>".$val->name." está descalificado, lo sentimos.</br>"."</div>";
                    $val->able =FALSE ;
                }
            }
            echo "</br>";
        }
        echo "</br>";
        echo "Lamentamos tantas pruebas, pero el organizador del torneo, Pablo Bejarano, lo ordenó así.";
        echo "</br>";
        echo "Ahora, la última prueba para determinar si son dignos, damas y caballeros, sigamos.";
        echo "</br>";
        echo "</br>";
        foreach ($this->trainerInput as $val){
            foreach ($val->pokemon as $vaal){
                if ($vaal->gender != ""){
                    echo "<div style='color:green;'>".$val->name." tu pokemon ".$vaal->name." no es legendario, puede competir: ".$vaal->gender." </br>"."</div>";
                }else{
                    echo "<div style='color:red;'>".$val->name."!!!!! tu pokemon ".$vaal->name." es legendario!!!!!!!!!!!!! </br>"."</div>";
                    echo "<div style='color:red;'>".$val->name." está descalificado, te vas a casa por Hackero.</br>"."</div>";
                    $val->able =FALSE ;
                }
            }
            echo "</br>";
        }
        echo "</br>";
        echo "Excelente, enhorabuena a los competidores que pasaron los TESTS!!!!!!!!!!!";
        echo "</br>";
        echo "</br>";
        echo "</br>";
        echo "</br>";
        echo "</br>";
        echo "</br>";
        echo "Ahora podemos iniciar con el Torneo Pokemon, a batallar se ha dicho!!";
        echo "</br>";
        echo "</br>";
        foreach ($this->trainerInput as $val){
            if($val->able==TRUE){
                echo $val->name." puede competir </br>";
                $this->totalVar($val);
            }else{
                echo $val->name." NO puede competir, aquí está tu maleta.</br>";
            }
        }
        echo "<------------------------------------------------------------------------------------>";
    }
    function totalVar($trainer){
    echo "<div style='-webkit-border-radius: 20px; -moz-border-radius: 20px; border-radius: 20px;margin-bottom:10px; background-color:rgba(0,0,0,0.2);' id='contentTrainer'><div style='color:red;'></br>Trainer Card</div></br>";
    echo "";
    echo "Trainer : ";
    echo "<strong>".$trainer->getName().".</strong></br>";
    echo "Medals : ";
    echo "<strong>".$trainer->getMedals().".</strong></br>";
    echo "Age : ";
    echo "<strong>".$trainer->getAge().".</strong></br>";
    echo "City : ";
    echo "<strong>".$trainer->getTown().".</strong></br>";
    echo "Pokemon : </br></br>";
    foreach ($trainer->getPokemon() as $val){
        echo "<strong>"."-->".$val->name.":</strong> </br>";
        echo "type: ".$val->type->name.".</br>";
        echo "gender: ".$val->gender.".</br>";
        echo "hp: ".$val->hp.".</br>";
        echo "speed: ".$val->speed."</br>";
        echo "level: ".$val->level."</br>";
        echo "Attacks: </br>";
        foreach ($val->attacks as $val2) {
            echo "->".$val2->name."  --  ";
            echo "type: ".$val2->type."  --  ";
            echo "pp: ".$val2->pp."  --  ";
            echo "power: ".$val2->power.". ";
            echo "</br>";
        }
        echo "</br></br>";
    }
    echo "</div>";
}

    function getBestAttack($pok1,$pok2){
    $e1 = 0;
    $e2 = 0;
    $e3 = 0;
    $e4 = 0;
    $lengthArray = count($pok2->getType()->weaknesses);
    for($i=0;$i<$lengthArray;$i++){
        if ( $pok2->getType()->weaknesses[$i] == $pok1->getAttack1()->type ){
            echo "el ataque '".$pok1->getAttack1()->name."' de '".$pok1->getName()."' es efectivo contra '".$pok2->getName()."'";
            $e1=1;
            echo "</br>";
        }
        if ( $pok2->getType()->weaknesses[$i] == $pok1->getAttack2()->type ){
            echo "el ataque '".$pok1->getAttack2()->name."' de '".$pok1->getName()."' es efectivo contra '".$pok2->getName()."'";
            $e2 = 1;
            echo "</br>";
        }
        
        if ( $pok2->getType()->weaknesses[$i] == $pok1->getAttack3()->type ){
            echo "el ataque '".$pok1->getAttack3()->name."' de '".$pok1->getName()."' es efectivo contra '".$pok2->getName()."'";
            $e3 = 1;
            echo "</br>";
        }
        if ( $pok2->getType()->weaknesses[$i] == $pok1->getAttack4()->type ){
            echo "el ataque '".$pok1->getAttack4()->name."' de '".$pok1->getName()."' es efectivo contra '".$pok2->getName()."'";
            $e4 = 1;
            echo "</br>";
        }
    
}
    $binVal = $e1.$e2.$e3.$e4 ;
    
    //echo $bin;
    
    $valueBin = $this->getValBin($binVal);
    echo "</br>";
    echo $valueBin;
    echo "</br>";
    echo "</br>";
    
}
    function getValBin($bin){
        switch ($bin){
            case '0000':
               return "case 1 : '0000'";
                break;
            case '0001':
               return "case 2 : '0001'";
                break;
            case '0010':
               return "case 3 : '0010'";
                break;
            case '0011':
                return "case 4 : '0011'";
                break;
            case '0100':
                return "case 5 : '0100'";
                break;
            case '0101':
                return "case 6 : '0101'";
                break;
            case '0110':
                return "case 7 : '0110'";
                break;
            case '0111':
                return "case 8 : '0111'";
                break;
            case '1000':
                return "case 9 : '1000'";
                break;
            case '1001':
                return "case 10 : '1001'";
                break;
            case '1010':
                return "case 11 : '1010'";
                break;
            case '1011':
                return "case 12 : '1011'";
                break;
            case '1100':
                return "case 13 : '1100'";
                break;
            case '1101':
                return "case 14 : '1101'";
                break;
            case '1110':
                return "case 15 : '1110'";
                break;
            case '1111':
                return "case 16 : '1111'";
                break;
        }
    }
}
