<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Electric type attacks
$thunderShock = new Attack("Thunder Shock","electric","15","70");
$electroBall = new Attack("Electro Ball","electric","16","90");

//normal type attacks
$quickAttack = new Attack("Quick Attack","normal","48","40");
$quickAttack2 = new Attack("Quick Attack","normal","48","40");
$extremeSpeed = new Attack("Extreme Speed","normal","8","80");
$extremeSpeed2 = new Attack("Extreme Speed","normal","8","80");
$hyperBeam = new Attack("Hyper Beam","normal","8","150");
$hyperBeam2 = new Attack("Hyper Beam","normal","8","150");
$megaPunch = new Attack("Mega Punch","normal","32","80");
$echoedVoice = new Attack("Echoed Voice","normal","24","40");
$bodySlam = new Attack("Body Slam", "normal", "24", "85");
$bodySlam2 = new Attack("Body Slam", "normal", "24", "85");

//Fire type attacks
$fireBlast = new Attack("Fire Blast","fire","8","110");
$fireBlast2 = new Attack("Fire Blast","fire","8","110");

//steel type attacks
$ironTail = new Attack("Iron Tail","steel","20","80");
$ironTail2 = new Attack("Iron Tail","steel","20","80");

//Dragon type attacks
$Outrage = new Attack("Outrage","dragon","16","120");
$Outrage2 = new Attack("Outrage","dragon","16","120");
$Outrage3 = new Attack("Outrage","dragon","16","120");
$dragonPulse = new Attack("Dragon Pulse","dragon","20","105");
$dragonPulse2 = new Attack("Dragon Pulse","dragon","20","105");

//Fighting type attacks
$auraSphere = new Attack("Aura Sphere","fighting","32","80");
$focusPunch = new Attack("Focus Punch","fighting","32","150");
$focusPunch2 = new Attack("Focus Punch","fighting","32","150");
$focusPunch3 = new Attack("Focus Punch","fighting","32","150");
$focusPunch4 = new Attack("Focus Punch","fighting","32","150");

//Poison type attacks
$gunkShot = new Attack("Gunk Shot","poison","8","120");
$gunkShot2 = new Attack("Gunk Shot","poison","8","120");
$sludgeWave = new Attack("Slude Wave","poison","16","95"); 

//Psychic type attacks
$zenHeadbutt = new Attack("Zen Headbutt","psychic","24","80");
$zenHeadbutt2 = new Attack("Zen Headbutt","psychic","24","80");
$zenHeadbutt3 = new Attack("Zen Headbutt","psychic","24","80");
$psyShock = new Attack("Psyshock","psychic","16","80");
$psyShock2 = new Attack("Psyshock","psychic","16","80");
$dreamEater = new Attack("Dream Eater","psychic","24","100");

//Fairy type attacks
$moonblast = new Attack("Moon Blast","fairy","24","95");
$dazzlingGleam = new Attack("Dazzling Gleam","fairy","16","80");

//Rock type attacks
$rockSlide = new Attack("Rock Slide","rock","16","75");
$rockSlide2 = new Attack("Rock Slide","rock","16","75");
$rockSlide3 = new Attack("Rock Slide","rock","16","75");

//Dark type attacks
$knockOff = new Attack("Knock Off","dark","32","65");
$knockOff2 = new Attack("Knock Off","dark","32","65");
$darkPulse = new Attack("Dark Pulse", "dark","24","120");
$darkPulse2 = new Attack("Dark Pulse", "dark","24","120");
$darkPulse3 = new Attack("Dark Pulse", "dark","24","120");

//Ice type attacks
$iceBeam = new Attack("Ice Beam","ice","16","90");
$iceBeam2 = new Attack("Ice Beam","ice","16","90");
$iceBeam3 = new Attack("Ice Beam","ice","16","90");
$blizzard = new Attack("Blizzard", "ice", "8", "110");

//Ground type attacks
$dig = new Attack("Dig","ground","16","95");
$earthquake = new Attack("Earthquake","ground","16","100");
$earthquake2 = new Attack("Earthquake","ground","16","100");
$earthquake3 = new Attack("Earthquake","ground","16","100");

//flying type attacks
$fly = new Attack("Fly","flying","24","100");
$fly2 = new Attack("Fly","flying","24","100");
$braveBird = new Attack("Brave Bird", "flying", "24", "120");

//Water type attacks
$hydroPump = new Attack("Hydro Pump","water", "15", "120");
$waterPulse = new Attack("Water Pulse", "water", "32", "75");

//Grass type attacks
$frenzyPlant = new Attack("Frenzy Plant", "grass", "8", "150");

//Bug type attacks
$megaHorn = new Attack("MegaHorn", "bug", "16", "120");

//Rock type attakcs
$stoneEdge = new Attack("Stone Edge", "rock", "8", "100");

//Steel type attacks
$sunsteelStrike = new Attack("Sunsteel Strike", "steel", "8", "110");

//Ghost type attacks
$shadowBall = new Attack("Shadow Ball", "ghost", "24", "100");